
const WebSocketInterface = require('./ws-interface');
 
class WebSocketProtocol extends WebSocketInterface {
	
	constructor (server) {
		super (server);
		
		this.queue = [];
		this.timer = null;
	}
	
	push (stream, filter, payload) {
		this.queue.push({
			stream,
			filter,
			payload
		});
	}
	
	async shift () {
		while (this.queue.length) {
			const message = this.queue.shift();
// 			console.log("Broadcast → ", message);
			await this.broadcast(message);
		}
	}
	
	senderStart (interval = 500) {
		const sender = async () => {
			if (this.timer) {
				await this.shift();
				this.timer = setTimeout(sender, interval);
			}
		}
		
		this.timer = setTimeout(sender, interval);
	}
	
	senderStop () {
		this.timer = null;
	}
	
};

module.exports = WebSocketProtocol;
