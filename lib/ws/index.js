const WebSocket = require('ws');
const jwt = require('jsonwebtoken');
const WebSocketProtocol = require('./ws-protocol');

function createWsInterface (server) {
	return new WebSocketProtocol(server);
}

module.exports = createWsInterface;
