const WebSocket = require('ws');
const jwt = require('jsonwebtoken');
const cfg = require('../config');
const db = require('../db');
const createToken = require('../create-token');

const jwt_secret = cfg.server.secret;

class WebSocketInterface {
	
	constructor (server) {
		this.connections = [];
		
		if (server) {
			this.server(server);
		}
	}
	
	async verify (conn) {
		const credentials = conn.jwt;
		try {
			return jwt.verify(credentials, jwt_secret);
		} catch (err) {
			if (err.name == 'TokenExpiredError') {
				console.log("WS TOKEN EXPIRED");
				// Try to renew token
				const payload = jwt.verify(credentials, jwt_secret, {ignoreExpiration: true});
				const userDetails = await db.user.validate({ $user_id: payload.user_id });
				if (userDetails) {
					// Send a new JWT object to the client
					const jwt = createToken(userDetails);
					conn.ws.send(JSON.stringify({
						type: "authentication",
						jwt
					}));
					return true
				}
				// Dropthrough
			}
			console.log("WS ERROR", err);
			// Dropthrough
		}
		console.log("WS VERIFY DROPTHROUGH");
		return false;
	}
	
	server(server) {
		this.webSocketServer = new WebSocket.Server({server});
		
		this.webSocketServer.on('connection', (ws, req) => {
			
			this.connections.push({
				req: req,
				jwt: null,
				subscriptions: [],
				ws
			});
			
			ws.on('message', (message) => {
				console.log("WS MESSAGE", message);
				const data = JSON.parse(message);
				if (data) {
					switch (data.type) {
						case "authentication":
							this.connections
							.filter(conn => conn.ws == ws)
							.map(conn => conn.jwt = data.jwt);
							console.log("WS AUTH RECEIVED");
							console.log(this.connections.map(c => {return {jwt: c.jwt, subscriptions: c.subscriptions}}));
							break;
						case "subscription":
							this.connections
							.filter(conn => conn.ws == ws)
							.forEach(conn => {
								const isAlreadySubscribed =
									conn.subscriptions
									.filter(subs => subs.channel == data.channel)
									.length;
								
								if (!isAlreadySubscribed) {
									const spec = data.channel.split("/", 2);
									const stream = spec[0];
									const filter = spec[1].split(";");
									const subscription = {
										channel: data.channel,
										stream,
										filter
									}
									
									conn.subscriptions.push(subscription);
								}
							});
							console.log("WS SUBSCRIPTION RECEIVED", data);
							
							break;
						case "unsubscription":
							this.connections
							.filter(conn => conn.ws == ws)
							.forEach(conn => {
								conn.subscriptions =
									conn.subscriptions.filter(subs => data.channel != subs.channel);
							});
							console.log("WS UNSUBSCRIPTION RECEIVED", data);

							break;
						default:
							console.warn("Unrecognised packet type", data.type);
					}
				}
			});
			
			ws.on('open', () => {
				console.log("WS OPEN", req.url);
			});
			
			ws.on('close', async () => {
				console.log("WS CLOSE", req.url);
				this.connections = this.connections.filter(conn => conn.ws != ws);
			});
		});
		
		return this;
	}
	
	async broadcast ({stream, filter, payload}) {
		const connections = this.connections.filter(conn => conn.ws.readyState == WebSocket.OPEN);
		
		if (!Array.isArray(filter)) {
			filter = [filter];
		}
		
		for (const conn of connections) {
			const isValid = await this.verify(conn);
			if (isValid) {
				conn.subscriptions
				.filter(subs => (!stream || subs.stream == stream) &&
					(!filter || !filter.length || filter.some(item => subs.filter.includes(item))))
				.forEach(subs => {
					conn.ws.send(JSON.stringify({
						type: "subscription",
						channel: subs.channel,
						payload
					}));
// 					conn.ws.send(JSON.stringify(payload));
// 					console.log("WS SEND", stream, filter, JSON.stringify(payload));
				});
			} else {
				// Get rid of clients whose auth is no longer valid
				console.log("WS CLOSE UNAUTHORISED CLIENT");
				this.signalClose(conn.ws);
			}
		}
			
	}
	
	// TODO No longer used, remove?
	async publish ({stream, filter}, payload) {

		const connections = this.connections.filter(conn => conn.ws.readyState == WebSocket.OPEN);
		
		for (const conn of connections) {
			const isValid = await this.verify(conn);
			if (isValid) {
				conn.subscriptions
				.filter(subs => (!stream || subs.stream == stream) &&
					(!filter || !filter.length || filter.some(item => subs.filter.includes(item))))
				.forEach(subs => {
					conn.ws.send(JSON.stringify({
						type: "subscription",
						channel: subs.channel,
						payload
					}));
				});
			} else {
				// Get rid of clients whose auth is no longer valid
				console.log("WS CLOSE UNAUTHORISED CLIENT");
				this.signalClose(conn.ws);
			}
		}
			
	}
	
	async signalClose (ws) {
		for (const conn of this.connections.filter(conn => conn.ws === ws)) {
			await conn.ws.send(JSON.stringify({
				type: "status",
				status: "connection_close"
			}));
		}
		ws.close();
	}
	
}

module.exports = WebSocketInterface;
