/**
 * @module Handle fixes
 */

const moment = require('moment');
const math = require('mathjs');
const db = require('../../lib/db');

function extract () {
	const attributes = Array.from(arguments);
	return {
		from: function () {
			const objects = Array.from(arguments);
			for (let object of objects) {
				if (object instanceof Object) {
					for (let attribute of attributes) {
						if (object.hasOwnProperty(attribute)) {
							return object[attribute];
						}
					}
				}
			}
			return undefined;
		}
	};
}

function removeRecognisedAttributes (metadata) {
	// Remove attributes that we otherwise recognise
	const recognisedAttributes = [
		"token",
		"id",
		"latitude", "lat", "y",
		"longitude", "lon", "x",
		"altitude", "elevation", "alt", "ele", "z",
		"timestamp", "tstamp", "t",
		"speed", "v",
		"direction", "bearing", "heading", "dir", "d"
	];
	
	for (let attribute of recognisedAttributes) {
		if (metadata.hasOwnProperty(attribute)) {
			delete metadata[attribute];
		}
	}
	
	return metadata;
}

async function setOrCheckLockKey (token_id, fixId) {
	const { lock_key } = await db.token.getLock({$token_id: token_id}) || {};
	
	if (lock_key && lock_key != fixId) {
		return false;
	}
	
	if (!lock_key && fixId) {
		/* Do not await */ db.token.setLock({
			$token_id: token_id,
			$lock_key: fixId
		});
	}
	
	return true;
}

math.createUnit("knot", {
	definition: (1852/3600) + " m/s",
	aliases: [ "knots", "kt", "kts" ]
});

function unitValue (magnitude, defaultUnit, targetUnit) {
	if (magnitude === undefined || magnitude === null) {
		return; // undefined
	}
	
	let mathObj = math.eval(magnitude);
	
	function outputNumber (unit) {
		return targetUnit
			? unit.to(targetUnit).toNumber()
			: unit.toSI().toNumber();
	}
	
	switch (math.typeof(mathObj)) {
		case "number":
			return outputNumber(math.unit(mathObj, defaultUnit));
		case "Unit":
			return outputNumber(mathObj);
		default:
			throw new Error("Unexpected magnitude type " + math.typeof(mathObj));
	}
}

/**
 * @function New fix middleware
 */
exports.new = async (req, res, next) => {
	try {
		
		let metadata;
		
		if (!req.body || (req.body && !req.body.hasOwnProperty("metadata"))) {
			metadata = Object.assign({}, req.query, req.body);
			removeRecognisedAttributes(metadata);
		} else {
			metadata = req.body.metadata;
		}
		
		const fixId = extract("id", "key").from(req.body, req.query);
		let fixData = {
			$token_id: req.params.token_id,
			$latitude: extract("latitude", "lat", "y").from(req.body, req.query),
			$longitude: extract("longitude", "lon", "x").from(req.body, req.query),
			$altitude: extract("altitude", "elevation", "alt", "ele", "z").from(req.body, req.query),
			$tstamp: extract("timestamp", "tstamp", "t").from(req.body, req.query),
			$speed: extract("speed", "v").from(req.body, req.query),
			$direction: extract("direction", "bearing", "heading", "dir", "d").from(req.body, req.query),
			$metadata: typeof metadata !== undefined ? JSON.stringify(metadata) : undefined
		}
		
		let units = {
			$latitude: {i: "deg", o: "deg"},
			$longitude: {i: "deg", o: "deg"},
			$altitude: {i: "m", o: "m"},
			$tstamp: {i: "ms", o: "ms"},
			$speed: {i: "m/s", o: "m/s"},
			$direction: {i: "deg", o: "deg"},
		};
		
		if (req.params.units) {
			req.params.units
			.split(/\s*;\s*/)
			.forEach( unitSpec => {
				const [ measurement, unit ] = unitSpec.split(/\s*:\s*/, 2);
				const key = "$" + measurement;
				if (units[key]) {
					units[key].i = unit
				}
			});
		}
		
		for (const key in fixData) {
			const unit = units[key];
			const value = fixData[key];
			if (unit) {
				fixData[key] = unitValue(value, unit.i, unit.o);
			}
		}
		
		if (fixData.$tstamp < 10000000000) {
			// Assume that we got a timestamp in seconds
			// and convert to milliseconds.
			fixData.$tstamp *= 1000;
		}
		
		const lockCheck = await setOrCheckLockKey(fixData.$token_id, fixId);
		if (req.user || lockCheck) {
			
			let fix_id = await db.fix.add(fixData);
// 			console.log("NEW FIX", fix_id, fixData);
// 			res.body = [{fix_id: fix}];
			
			// Do not wait until we process the websocket notifications
			res.status(201).json([{fix_id}]);
			
			const syntheticFixes = await db.fix.get({$fix_id: fix_id});
			syntheticFixes
			.forEach( fix => {
				fix.metadata = JSON.parse(fix.metadata);
				req.app.ws.push("fixes", fix.token_id, {add: [fix]});
			});
		} else {
			
// 			console.warn("FIX REJECTED (WRONG KEY)", fixId);
			res.status(202).json([{error: "wrong_token_key"}]);
			
		}
	} catch (error) {
		
		if (error.code == 'SQLITE_CONSTRAINT') {
			// The token does not exist
			res.status(404).end();
		} else {
			console.error(error);
			res.status(500).json();
		}
		
	}
	next();
};

exports.list = async (req, res, next) => {
	if (req.user || req.publicUser) {
		try {
			const params = {
				$token_id: req.params.token_id,
				$user_id: req.user && req.user.user_id,
				$limit: req.query.limit || req.query.l,
				$offset: req.query.offset || req.query.o,
				$tstamp0: req.query.tstamp0 || req.query.ts0 || req.query.t0,
				$tstamp1: req.query.tstamp1 || req.query.ts1 || req.query.t1
			}
			const fixList = (await db.fix.list(params)).map(fixData => {
				if (fixData.metadata) {
					fixData.metadata = JSON.parse(fixData.metadata);
				}
				return fixData;
			});
			res.json(fixList);
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		res.status(401).end();
	}
};


exports.geojson = {};

exports.geojson.list = async (req, res, next) => {
	if (req.user) {
		try {
			const params = {
				$token_id: req.params.token_id,
				$user_id: req.user.user_id,
				$limit: 500,
				$offset: 0
			}
			
			// Send HTTP headers
			res.set("Content-Type", "application/vnd.geo+json");
			res.set("Content-Disposition",
					`attachment; filename="${req.params.token_id}-fixes.geojson"`);
			
			// Send header
			res.write('{"type":"FeatureCollection","features":[');
			// Send body
			let fixList;
			do {
				fixList = (await db.fix.list(params))
				.map(fix => {
					const feature = {
						type: "Feature",
						geometry: {
							type: "Point",
							coordinates: [
								fix.longitude,
								fix.latitude
							]
						},
						properties: {
							...JSON.parse(fix.metadata),
							fix_id: fix.fix_id,
							token_id: req.params.token_id,
							timestamp: fix.tstamp,
							created: fix.created,
							altitude: fix.altitude,
							speed: fix.speed,
							direction: fix.direction
						}
					};
					return JSON.stringify(feature);
				})
				.join(",");
				
				if (fixList.length && params.$offset > 0) {
					fixList = ","+fixList;
				}
				
				res.write(fixList);
				
				params.$offset += params.$limit;
				
			} while (fixList.length)
			// Send footer
			res.write(']}');
			res.end();
			
			
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		res.status(401).end();
	}
};

exports.geojson.new = async (req, res, next) => {
	try {
// 		console.log("GEOJSON UPLOAD");
		// We need to keep local references to all the
		// stuff as this handler will be returning before
		// we are done processing.
		const token_id = req.params.token_id;
		const payload = Object.assign({}, req.body);
		const user = req.user;
		const app = req.app;
		
		let metadata;
		
		const processGeoJSON = async (element, callback) => {
// 			console.log("processGeoJSON");
			
			switch (element.type) {
				case "FeatureCollection":
// 					console.log("FeatureCollection");
					for (const feature of element.features) {
						await processGeoJSON (feature, callback);
					}
					break;
				case "Feature":
// 					console.log("Feature");
					switch (element.geometry && element.geometry.type) {
						case "Point":
// 							console.log("Point");
							let metadata = Object.assign({}, element.properties);
							removeRecognisedAttributes(metadata);
							
							let fixData = {
								$token_id: token_id,
								$latitude: Number(element.geometry.coordinates[1]),
								$longitude: Number(element.geometry.coordinates[0]),
								$altitude:
									Number(element.geometry.coordinates[2] ||
										extract("altitude", "elevation", "alt", "ele", "z").from(element.properties)),
								$tstamp:
									extract("timestamp", "tstamp", "time", "date", "datetime", "t")
										.from(element.properties),
								$speed: Number(extract("speed", "v").from(element.properties)),
								$direction:
									Number(extract("direction", "bearing", "dir", "d")
									.from(element.properties)),
								$metadata: typeof metadata !== undefined
									? JSON.stringify(metadata)
									: undefined
							};
							
							// Fix timestamps
							if (typeof fixData.$tstamp == 'number') {
								if (fixData.$tstamp < 10000000000) {
									// Assume that we got a timestamp in seconds
									// and convert to milliseconds.
									fixData.$tstamp *= 1000;
								}
							} else {
								const tstamp = moment(fixData.$tstamp);
								if (tstamp.isValid()) {
									fixData.$tstamp = tstamp.valueOf();
								} else {
									fixData.$tstamp = null;
								}
							}
							
							if (callback) {
								await callback(null, fixData);
							}
					}
			}
		};
		
// 		res.body = [];
		res.promise = processGeoJSON(payload, async (error, fixData) => {
			if (!error) {
				if (fixData) {
					const fix = await db.fix.add(fixData);
// 					console.log("NEW FIX", fix);
// 					res.body.push({fix_id: fix});
					return fix;
				}
			}
		});
		
// 		console.log("END GEOJSON UPLOAD");

		res.status(202).end();
		
		res.promise.then(async () => {
			// Signal for this token to be refreshed
			app.ws.push("tokens", user.user_id, { refresh: token_id });
			// Signal for other affected tokens to be refreshed
			const tokens = await db.token.listShared({
				$token_id: token_id,
				$user_id: user.user_id,
			});
			tokens
			.filter( token => token.permissions.includes('r') )
			.forEach( token =>
				app.ws.push("tokens", token.user_id, { refresh: token.v_token_id })
			);
		});
	} catch (error) {
		if (error.code == 'SQLITE_CONSTRAINT') {
			// The token does not exist
			res.status(404).end();
		} else {
			console.error(error);
			res.status(500).json();
		}
	}
	next();
};
