/**
 * @module Version middleware
 * 
 * Returns the API version
 */

module.exports = (versionText) => {
	return (req, res, next) => {
		res.json({version: versionText});
		next();
	}
};
