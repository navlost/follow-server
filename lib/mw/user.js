/**
 * @module User middleware
 */

const jwt = require('jsonwebtoken');
const db = require('../db');
const cfg = require('../config');
const createToken = require('../create-token');

const jwt_secret = cfg.server.secret;

exports.jwt = async (req, res, next) => {
	if (req.headers.authorization) {
		let [ scheme, credentials ] = req.headers.authorization.split(' ');
		
		if (/^Bearer$/i.test(scheme)) {
			
			try {
				let payload = jwt.verify(credentials, jwt_secret);
				req.user = payload;
				next();
				return;
			} catch (err) {
				if (err.name == 'TokenExpiredError') {
					// Try to renew token
					let payload = jwt.verify(credentials, jwt_secret, {ignoreExpiration: true});
					const userDetails = await db.user.validate({ $user_id: payload.user_id });
					if (userDetails) {
						const token = createToken(userDetails);
						res.set('x-jwt-token', token);
						req.user = payload;
						next();
						return
					}
				}
				res.status(401).json(err);
				return;
			}
		}
	} else {
		// If no authorisation, check to see if this instance has a public
		// user. If so, grant access as the public user.
		const publicUserDetails = await db.user.validate({ $user_id: "00000000" });
		if (publicUserDetails) {
			req.publicUser = { user_id: publicUserDetails.user_id, isPublic: true };
		}
	}
	
	// If no authentication, continue. Other middleware is
	// expected to check for the presence of req.user in
	// order to enforce authorisation.
	next();
};

exports.create = async (req, res, next) => {
	if (req.body.handle && req.body.password) {
		const metadata = req.body.metadata
			? JSON.stringify(req.body.metadata)
			: null;
		if (metadata === null || metadata.length < 500) { // FIXME max metadata length
			try {
				await db.user.add({
					$handle: req.body.handle,
					$password: req.body.password,
					$metadata: metadata
				})
				res.status(201).end();
				next();
			} catch (err) {
				console.error(err);
				// TODO Check for handle conflicts and return a 409
				res.status(500).json({error: 'database_error'});
			}
		} else {
			res.status(413).json({error: 'payload_too_large'});
		}
	} else {
		res.status(400).json({error: 'missing_required_data'});
	}
};

exports.login = async (req, res, next) => {
	if (req.body.handle && req.body.password) {
		try {
			const userData = {$handle: req.body.handle, $password: req.body.password};
			const userDetails = await db.user.login(userData);
			if (userDetails) {
				const token = createToken(userDetails);
				res.set('x-jwt-token', token);
				res.json({jwt: token});
			} else {
				res.status(401).json({error: 'bad_login'});
			}
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		res.status(400).json({error: 'missing_required_data'});
	}
	next();
};

exports.remove = async (req, res, next) => {
	if (req.user) {
		try {
			await db.user.remove({$user_id: req.user.user_id});
			res.status(204).end();
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		res.status(401).end();
	}
	next();
};

exports.get = {};

exports.get.handle = async (req, res, next) => {
	const userDetails = await db.user.validate({ $handle: req.params.handle });
	if (userDetails) {
		res.json({handle: userDetails.handle, user_id: userDetails.user_id});
	} else {
		res.status(404).end();
	}
	
	next();
};

