/**
 * @module Token middleware
 */

const db = require('../db');
const cfg = require('../config');

exports.create = async (req, res, next) => {
	if (req.user) {
		const metadata = JSON.stringify(req.body.metadata || "");
		if (metadata.length < 500) { // FIXME max metadata length
			try {
				const token_id = await db.token.add({$user_id: req.user.user_id, $metadata: metadata});
				res.status(201).json(token_id);
				
				if (token_id) {
					req.app.ws.push("tokens", req.user.user_id, {add: token_id});
				}
			} catch (err) {
				console.error(err);
				res.status(500).end();
			}
		} else {
			res.status(413).end();
		}
	} else {
		res.status(401).end();
	}
	next();
};

exports.update = async (req, res, next) => {
	if (req.user) {
		const metadata = JSON.stringify(req.body.metadata || "");
		if (metadata.length < 500) { // FIXME max metadata length
			try {
				const payload = {
					$token_id: req.params.token_id,
					$user_id: req.user.user_id,
					$metadata: metadata
				};
				
				if (payload.$token_id) {
					await db.token.update(payload);
					res.status(204).end();
					
					req.app.ws.push("tokens", req.user.user_id, {refresh: req.params.token_id});
				} else {
					res.status(400).end();
				}
			} catch (err) {
				console.error(err);
				res.status(500).end();
			}
		} else {
			res.status(413).end();
		}
	} else {
		res.status(401).end();
	}
	next();
};

exports.list = async (req, res, next) => {
	if (req.user || req.publicUser) {
		try {
			const params = {
				$user_id: req.user && req.user.user_id,
				$limit: req.query.l,
				$offset: req.query.o
			}
			
			const tokenList = (await db.token.list(params))
			.map(tokenData => {
				if (tokenData.metadata) {
					tokenData.metadata = JSON.parse(tokenData.metadata);
				}
				return tokenData;
			});
			
			res.json(tokenList);
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		// TODO There might be public tokens which are accessible to everyone
		// regardless of authentication. Until that is implemented, we just
		// return an empty list here.
		res.json([]);
	}
	next();
};

exports.get = async (req, res, next) => {
	if (req.user || req.publicUser) {
		try {
			const params = {
				$user_id: req.user && req.user.user_id,
				$token_id: req.params.token_id
			}
			const tokenData = await db.token.get(params);
			if (tokenData) {
				if (tokenData.metadata) {
					tokenData.metadata = JSON.parse(tokenData.metadata);
				}
				res.json(tokenData);
			} else {
				res.status(404).end();
			}
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		res.status(401).end();
	}
	next();
};

exports.remove = async (req, res, next) => {
	if (req.user) {
		try {
			const params = {
				$user_id: req.user.user_id,
				$token_id: req.params.token_id
			}
			
			const tokenList = await db.token.listShared(params);
			const changes = await db.token.remove(params);
			
			res.status(204).end();
			
			if (changes) {
				for (const token of tokenList) {
					req.app.ws.push("tokens", token.user_id, {remove: token.v_token_id});
				}
				req.app.ws.push("tokens", req.user.user_id, {remove: req.params.token_id});
			}
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		res.status(401).end();
	}
	next();
};

exports.share = {};

exports.share.new = async (req, res, next) => {
	if (req.user) {
		const params = {
			$token_id: req.params.token_id,
			$user_id: req.user.user_id,
			$recipient_id: req.params.recipient_id,
			$permissions: req.params.recipient_id == '00000000'
				? [...req.params.permissions].filter(p => /[rs]/.test).join("")
				: req.params.permissions,
			$expiry: Number(req.params.expiry) || null
		};
		
		const sharedTokenId = await db.token.share(params);
		
		res.status(201).send({ token_id: sharedTokenId });
		
		req.app.ws.push("tokens", req.params.recipient_id, {add: sharedTokenId});
	} else {
		res.status(401).end();
	}
	next();
};

exports.share.list = async (req, res, next) => {
	if (req.user) {
		try {
			const params = {
				$token_id: req.params.token_id,
				$user_id: req.user.user_id,
				$limit: req.query.l,
				$offset: req.query.o
			}
			
			const tokenList = (await db.token.listShared(params)).map(tokenData => {
				if (tokenData.metadata) {
					tokenData.metadata = JSON.parse(tokenData.metadata);
				}
				return tokenData;
			});
			res.json(tokenList);
		} catch (err) {
			console.error(err);
			res.status(500).end();
		}
	} else {
		res.status(401).end();
	}
	next();
};
