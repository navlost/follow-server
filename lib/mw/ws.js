// TODO No longer used, remove?

const db = require('../../lib/db');

exports.publish = { fix: {}, token: {} };

exports.publish.fix.new = async (req, res, next) => {
// 	console.log("BROADCAST", req.params.token, res.body && res.body.fix_id);
	
	async function worker () {
		if (Array.isArray(res.body)) {
			let payloads = {};
			
			for (const el of res.body) {
				if (el.fix_id) {
					const fixes = await db.fix.get({
						$fix_id: el.fix_id,
// 						$token_id: req.params.token_id
					});
					fixes
					.map(fix => {
						if (fix.metadata) {
							fix.metadata = JSON.parse(fix.metadata);
						}
						return fix;
					})
					.forEach(fix => {
// 						console.log("Fix", fix);
						if (payloads[fix.token_id]) {
							payloads[fix.token_id].add.push(fix);
						} else {
							payloads[fix.token_id] = {add: [ fix ]};
						}
					});
				}
			}
			
			for (const token_id in payloads) {
				req.app.ws.publish({stream: "fixes", filter: [ token_id ]}, payloads[token_id]);
			}
		}
	}
	
	/* If we are given a promise, wait for it before we signal,
	 * if there is no promise, then res.body already has everything
	 * we need and we can proceed.
	 */
	if (res.promise) {
		res.promise.then(worker);
	} else {
		worker();
	}
};

exports.publish.token.new = async (req, res, next) => {
// 	console.log("NEW WS CONNECTION", req.token, req.params);
	// TODO Publish token creation / deletion / update
};

exports.info = async (req, res, next) => {
	req.app.ws.broadcast({
		type: "broadcast",
		tstamp: Date.now()
	});
	res.send("OK\n");
	next();
}
