/**
 * @module Router map
 * 
 * Builds routes from an object.
 */

const cfg = require('./config');

const verbose = (process.env.NODE_VERBOSE !== undefined) ?
	process.env.NODE_VERBOSE
	: cfg.server.verbose;

module.exports = function (a, route) {
	route = route || '';
	for (let key in a) {
		switch (typeof a[key]) {
			// { '/path': { ... }}
			case 'object':
				if (!Array.isArray(a[key])) {
					this.map(a[key], route + key);
					break;
				} // else drop through
				// get: function(){ ... }
			case 'function':
				if (verbose) console.log('%s %s', key, route);
				this[key](route, a[key]);
				break;
			default:
				console.error(`Unknown type: ${typeof a[key]} for ${a[key]} (key: ${key})`);
				console.error(a, route);
		}
	}
};
