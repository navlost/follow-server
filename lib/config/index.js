/**
 * @module Configuration module
 * 
 * It will load the configuration from a YAML file.
 * The location of the YAML file is determined as
 * follows, in order of priority:
 * 
 * * If the FOLLOW_CONFIG_PATH environment variable
 *   exists, use it. If the path does not exist, no
 *   configuration is loaded (and the process will
 *   fail to start).
 * 
 * * If we are in test mode (NODE_ENV=test), try
 *   ./config.test.yaml, ./config.test.yml,
 *   ./config.yaml, ./config.yml. Use the first one
 *   found which is not empty.
 * 
 * * If we are in development mode (NODE_ENV=development),
 *   try ./config.dev.yaml, ./config.dev.yml,
 *   ./config.yaml, ./config.yml. Use the first one
 *   found which is not empty.
 * 
 * * In production mode (NODE_ENV=production),
 *   use ./config.yaml or ./config.yml.
 * 
 */

const fs = require('fs');
const YAML = require('yaml');

var config = {};

function checkForDefaultSecrets () {
	const banned = "0000000000000000000000000000000000000000000000000000000000000000";
	
	function recursor (obj) {
		for (const key in obj) {
			if (obj[key] instanceof Object) {
				recursor(obj[key]);
			} else if (key == "secret" && obj[key] == banned) {
				throw new Error("Refusing to start with default secrets. Please provide your own");
			}
		}
	}
	
	if (process.env.NODE_ENV == "production" || !process.env.NODE_ENV) {
		recursor(config);
	}
};

function readConfig () {
	let paths = [ "./config.yaml", "./config.yml" ];
	
	if (process.env.FOLLOW_CONFIG_PATH) {
		paths = [ process.env.FOLLOW_CONFIG_PATH ];
	} else {
		switch (process.env.NODE_ENV) {
			case "test":
				paths.unshift("./config.test.yaml", "./config.test.yml")
				break;
			case "development":
				paths.unshift("./config.dev.yaml", "./config.dev.yml")
				break;
			case "production":
				// Already defined
				break;
		}
	}
	
	for (const path of paths) {
		try {
			const contents = fs.readFileSync(path, 'utf8');
			if (contents) {
				config = YAML.parse(contents);
				if (config) {
					checkForDefaultSecrets();
					break;
				}
			}
		} catch (err) {
			if (err.code == 'ENOENT') {
				continue;
			}
			throw err;
		}
	}
};

readConfig();

module.exports = config;
