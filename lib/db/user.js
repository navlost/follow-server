/**
 * @module Sqlite authentication / authorisation database backend
 */

const crypto = require('crypto');
const dbf = require('./db-functions');
const cfg = require("../config");
const dbSecret = cfg.backend.db.secret;

if (process.env.NODE_ENV == "test") {
	const clearAllQry = `
	DELETE FROM users WHERE user_id != '00000000';
`;
	
	exports.clearDatabase = async function (db) {
		await dbf.exec(db, clearAllQry);
	};
}

const createUserTableQry = `
CREATE TABLE IF NOT EXISTS users (
	user_id CHAR(8) NOT NULL PRIMARY KEY,
	handle VARCHAR(64) NOT NULL UNIQUE,
	secret VARCHAR(64) NOT NULL,
	created INTEGER NOT NULL DEFAULT (strftime('%s.%f', 'now')*1000),
	metadata VARCHAR(500)
);

CREATE INDEX IF NOT EXISTS handle_idx ON users (handle);

-- This is the “public” pseudo-user. Real users can share tokens
-- with this user—those tokens will then be accessible to clients
-- whether they are authenticated or not. If a user has given the
-- re-share permission on a token shared with the public pseudo-user,
-- that token will appear on token listings whether the user is
-- authenticated or not. Otherwise, the token is accessible by all
-- but is not shown on token listings.
INSERT OR IGNORE
INTO users (user_id, handle, secret)
VALUES ('00000000', '', '');
`;

const createUserQry = `
INSERT INTO users (user_id, handle, secret, metadata)
VALUES ($user_id, $handle, $secret, $metadata);
`;

const loginUserQry = `
SELECT user_id, handle, created, metadata
FROM users
WHERE handle = $handle AND secret = $secret;
`;

const validateUserQry = `
SELECT user_id, handle, created, metadata
FROM users
WHERE ($handle IS NULL AND user_id = $user_id)
	OR ($user_id IS NULL AND LOWER(handle) = LOWER($handle));
`;

const removeUserQry = `
DELETE
FROM users
WHERE user_id = $user_id;
`;

const statements = {};
exports.initialiseDatabase = async function (db) {
	await dbf.exec(db, createUserTableQry);
	statements.createUser = await dbf.prepare(db, createUserQry);
	statements.loginUser = await dbf.prepare(db, loginUserQry);
	statements.validateUser = await dbf.prepare(db, validateUserQry);
	statements.removeUser = await dbf.prepare(db, removeUserQry);
};

exports.add = async function (userData) {
	const user_id = crypto.randomBytes(4);
	const secret = crypto.createHmac('sha256', dbSecret);
	secret.update(userData.$password);
	
	const params = {
		$user_id: user_id.toString('hex'),
		$handle: userData.$handle,
		$secret: secret.digest('hex'),
		$metadata: userData.$metadata
	}
	return await dbf.run(statements.createUser, params);
};

exports.login = async function (userData) {
	const secret = crypto.createHmac('sha256', dbSecret);
	secret.update(userData.$password);
	
	const params = {
		$user_id: userData.$user_id,
		$handle: userData.$handle,
		$secret: secret.digest('hex')
	};
	const res = await dbf.all(statements.loginUser, params);
	return res && res[0];
};

exports.validate = async function (userData) {
	res = await dbf.all(statements.validateUser, userData);
	return res && res[0];
};

exports.remove = async function (userData) {
	return await dbf.run(statements.removeUser, userData);
};
