/**
 * @module Wrapper around query functions.
 * 
 * Uses promises rather than callbacks.
 */

function exec (db, query) {
	return new Promise ((resolve, reject) => {
		db.spatialite(function (err) {
			if (err) {
				reject(err);
			} else {
				db.exec(query, function (err) {
					if (err) {
						reject(err);
					} else {
						resolve();
					}
				});
			}
		});
	});
};

function prepare (db, query, name) {
	return new Promise ((resolve, reject) => {
		const stmt = db.prepare(query, (err) => {
			if (err) {
				reject(err);
			} else {
				resolve(stmt);
			}
		});
	});
}

function run (statement, data, property = "lastID" /* or "changes" */) {
	return new Promise ((resolve, reject) => {
		statement.run(data, function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(this[property]);
			}
		});
	});
}

function all (statement, data) {
	return new Promise ((resolve, reject) => {
		statement.all(data, function (err, rows) {
			if (err) {
				reject(err);
			} else {
				resolve(rows);
			}
		});
	});
}

function limitOffset(data) {
	const $limit = (data.$limit && !isNaN(Number(data.$limit)))
		? Math.min(Number(data.$limit), 1000)
		: 100;
	const $offset = (data.$offset && !isNaN(Number(data.$offset)))
		? Math.max(data.$offset, 0)
		: 0;
	return {$limit, $offset};
}
	
module.exports = {
	exec,
	prepare,
	run,
	all,
	limitOffset
};
