
const URL = require('url');
const path = require('path');
const Datastore = require('spatialite');
const cfg = require("../config");

const dbUrl = URL.parse(cfg.backend.db.url);

var db;

if (dbUrl.protocol != "sqlite:" && dbUrl.protocol != "spatialite:") {
	throw new Error("Wrong database type. Expected sqlite or spatialite, got " + dbUrl.protocol);
}

if (dbUrl.host) {
	throw new Error("Spatialite databases must be local");
}

if (dbUrl.path) {
	const dbPath = path.resolve(__dirname, "../../", dbUrl.path.replace(/^\//, "")).normalize();
	db = new Datastore.Database(dbPath);
	exports.dbPath = dbPath;
} else {
	db = new Datastore.Database(":memory:");
}


/*
 * Import specific queries and bind
 * to export symbols.
 */

const spatialite = require('./spatialite');
exports.fix = spatialite.fix;
exports.token = spatialite.token;
exports.user = require('./user');

exports.initialiseDatabase = async () => {
	await exports.user.initialiseDatabase(db);
	await spatialite.initialiseDatabase(db);
};

if (process.env.NODE_ENV == "test") {
	exports.clearDatabase = async function () {
		await exports.user.clearDatabase(db);
		await spatialite.clearDatabase(db);
	};
}
