/**
 * @module Spatialite database backend
 */

const crypto = require('crypto');
const dbf = require('./db-functions');

if (process.env.NODE_ENV == "test") {
	const clearAllQry = `
	DELETE FROM tokens;
	DELETE FROM fixes;
`;
	
	exports.clearDatabase = async function (db) {
		await dbf.exec(db, clearAllQry);
	};
}



/**************************
 * Table creation queries
 **************************/


const createTokenTableQry = `
CREATE TABLE IF NOT EXISTS tokens (
	v_token_id CHAR(16) NOT NULL PRIMARY KEY,  -- “Virtual” token
	r_token_id CHAR(16) NOT NULL,  -- “Real” token (goes into fixes.r_token_id)
	user_id CHAR(8) NOT NULL,      -- Owner of “virtual token”
	permissions CHAR(8) NOT NULL DEFAULT 'DUcruds',
	created INTEGER NOT NULL DEFAULT (strftime('%s.%f', 'now')*1000),
	expiry INTEGER DEFAULT NULL,
	lock_key VARCHAR(64) DEFAULT NULL,
	metadata VARCHAR(500),
	FOREIGN KEY (user_id) REFERENCES users (user_id)
		ON UPDATE CASCADE ON DELETE CASCADE
	FOREIGN KEY (r_token_id) REFERENCES tokens (v_token_id)
		ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS r_token_id ON tokens (r_token_id);
PRAGMA foreign_keys = ON;
`;

/*
 * NOTE The FOREIGN KEY constraint here references tokens (v_token_id)
 * when what we actually care about is the r_token_id. However, we need
 * to do it like this because tokens.r_token_id is neither the primary
 * key nor subject to a unique constraint (nor can it be).
 * What we do here is take advantage of the fact that every r_token_id
 * is also a perfectly valid v_token_id.
 * NOTE There is a risk here that a fix referencing a vtoken could be
 * inserted, but that should not happen if using the prepared statements
 * in this file.
 */
const createFixesTableQry = `
CREATE TABLE IF NOT EXISTS fixes (
	fix_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	r_token_id CHAR(16) NOT NULL,
	tstamp INTEGER NOT NULL,
	created INTEGER NOT NULL DEFAULT (strftime('%s.%f', 'now')*1000),
	altitude DOUBLE,
	speed DOUBLE,
	direction DOUBLE,
	metadata VARCHAR(500),
	FOREIGN KEY (r_token_id) REFERENCES tokens (v_token_id)
		ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS token_idx ON fixes (r_token_id);
CREATE INDEX IF NOT EXISTS tokenuid_idx ON fixes (r_token_id, fix_id);
CREATE INDEX IF NOT EXISTS tstamp_idx ON fixes (tstamp);
SELECT InitSpatialMetaData();
SELECT AddGeometryColumn('fixes', 'location',
	4326, 'POINT', 'XY', 1);
`;



/**************************
 * Token queries
 **************************/


/* 
 * Used to create a new token by its owner.
 * See shareTokenQry for creation of vtokens.
 */
const createTokenQry = `
INSERT INTO tokens (v_token_id, r_token_id, user_id, metadata)
VALUES ($token_id, $token_id, $user_id, $metadata);
`;

/*
 * In order to update a token, the calling user
 * must have the 'u' permission on that token.
 */
const updateTokenQry = `
UPDATE tokens
SET
	metadata = $metadata
WHERE v_token_id = $token_id
	AND user_id = $user_id
	AND instr(permissions, 'u') > 0;
`;

/*
 * If we are dealing with lock keys it is
 * because the user is not otherwise
 * authenticated, so we cannot do a match
 * against token *and* user.
 * 
 * Locks always go against the vtoken not
 * the rtoken, as each user has its own
 * “secret”.
 */
const getTokenLockQry = `
SELECT lock_key
FROM tokens
WHERE v_token_id = $token_id;
`;

// See comment above
const setTokenLockQry = `
UPDATE tokens
SET
	lock_key = $lock_key
WHERE v_token_id = $token_id;
`;

/*
 * NOTE Do we need to list all the tokens
 * or only those the user has read access to?
 * 
 * I think we list them all. No read access only
 * comes into play when trying to read fixes from
 * them.
 */
const listTokensQry = `
SELECT v_token_id as token_id, user_id, permissions, created, expiry, metadata,
	CASE r_token_id
		WHEN v_token_id THEN NULL
		ELSE (
			SELECT u.handle
			FROM tokens tt
			INNER JOIN users u
			ON tt.user_id = u.user_id
			WHERE tt.v_token_id = t.r_token_id)
	END shared_by
FROM tokens t
WHERE
	user_id = $user_id
	OR (user_id = '00000000' AND instr(permissions, 's'))
ORDER BY created DESC
LIMIT $limit OFFSET $offset;
`;

/*
 * NOTE See the comment above.
 */
const getTokenQry = `
SELECT v_token_id as token_id, user_id, permissions, created, expiry, metadata,
	CASE r_token_id
		WHEN v_token_id THEN NULL
		ELSE (
			SELECT u.handle
			FROM tokens t
			INNER JOIN users u
			ON t.user_id = u.user_id
			WHERE t.v_token_id = r_token_id)
	END shared_by
FROM tokens
WHERE token_id = $token_id
	AND (user_id = $user_id OR user_id = '00000000');
`;

/*
 * When the token being removed is the rtoken
 * (vtoken == rtoken), the foreign key constraint
 * will cause all its shares to be deleted too.
 * 
 * The $token_id always refers to the vtoken,
 * then the $user_id has to match either
 * the vtoken's user or the corresponding
 * rtoken's user.
 * 
 * If $user_id = vtokens.user_id, the recipient
 * is deleting this from his list of shares.
 * 
 * If $user_id = rtokens.user_id, the owner
 * of the rtoken is unsharing it with
 * the recipient.
 * 
 * If $user_id = "00000000" (the public user),
 * ensure that no deletion takes place.
 */
const removeTokenQry = `
DELETE
FROM tokens
WHERE v_token_id IN (
	SELECT vtokens.v_token_id
	FROM tokens vtokens
		INNER JOIN tokens AS rtokens
		ON vtokens.r_token_id = rtokens.v_token_id
	WHERE vtokens.v_token_id = $token_id
		AND $user_id != '00000000'
		AND (
			(vtokens.user_id = $user_id)
			OR
			(rtokens.user_id = $user_id)
		)
);
`;

const shareTokenQry = `
INSERT INTO tokens (v_token_id, r_token_id, user_id, permissions, expiry, metadata)
	SELECT $v_token_id, r_token_id, $recipient_id, $permissions, $expiry, metadata
	FROM tokens
	WHERE v_token_id = $token_id
		AND user_id = $user_id
		AND instr(permissions, 's');
`;

const listSharedTokensQry = `
SELECT v.v_token_id, v.r_token_id, v.user_id, u.handle, v.permissions, v.expiry, v.metadata
FROM tokens v
	INNER JOIN tokens r
	ON v.r_token_id = r.r_token_id
	INNER JOIN users u
	ON v.user_id = u.user_id
WHERE
	$user_id = r.user_id
	AND (
		$token_id IS NULL -- Return everything this user has shared
		OR
		$token_id = r.v_token_id -- Return only the shares for one specific token
	)
	AND v.v_token_id <> v.r_token_id
ORDER BY v.created DESC
LIMIT $limit OFFSET $offset;
`;



/**************************
 * Fix queries
 **************************/


/*
 * Here we take the v_token_id as input, use it to
 * look up the r_token_id and check that we do have
 * write permission, and write the r_token_id into
 * the fix.
 */
const createFixQry = `
INSERT INTO fixes (location, r_token_id, tstamp, altitude, speed, direction, metadata)
VALUES (
	MakePoint($longitude, $latitude, 4326),
	(SELECT r_token_id FROM tokens WHERE v_token_id = $token_id AND instr(permissions, 'c')),
	$tstamp, $altitude, $speed, $direction, $metadata
);
`;

/*
 * Fixes are listed based on vtoken, and it is the vtoken
 * that is returned in the select. The vtoken must have
 * the 'r' (read) permission set for this to succeed.
 * 
 * We return the v_token_id as token_id as the vtoken/rtoken
 * existence is supposed to be an implementation detail
 * opaque to the caller.
 */
const getFixQry = `
SELECT
	fix_id, t.v_token_id AS token_id,
	Y(location) AS latitude, X(location) AS longitude,
	tstamp, f.created, altitude, speed, direction, f.metadata
FROM fixes f
	INNER JOIN tokens t
	ON f.r_token_id = t.r_token_id AND instr(permissions, 'r')
WHERE fix_id = $fix_id AND ($token_id IS NULL OR t.v_token_id = $token_id);
`;

/*
 * Fixes are listed based on vtoken, and it is the vtoken
 * that is returned in the select. The vtoken must have
 * the 'r' (read) permission set for this to succeed.
 * 
 * We return the v_token_id as token_id as the vtoken/rtoken
 * existence is supposed to be an implementation detail
 * opaque to the caller.
 */
const listFixesQry = `
SELECT
	fix_id, v_token_id AS token_id,
	Y(location) AS latitude, X(location) AS longitude,
	tstamp, f.created, altitude, speed, direction, f.metadata
FROM fixes f
	INNER JOIN tokens t
	ON f.r_token_id = t.r_token_id AND instr(t.permissions, 'r')
WHERE t.v_token_id = $token_id
	AND ($tstamp0 IS NULL OR tstamp >= $tstamp0)
	AND ($tstamp1 IS NULL OR tstamp <= $tstamp1)
ORDER BY tstamp DESC
LIMIT $limit OFFSET $offset;
`;

const statements = {};
exports.initialiseDatabase = async function (db) {
	// Table creation
	await dbf.exec(db, createTokenTableQry);
	await dbf.exec(db, createFixesTableQry);
	
	// Tokens
	statements.createToken = await dbf.prepare(db, createTokenQry);
	statements.updateToken = await dbf.prepare(db, updateTokenQry);
	statements.listTokens = await dbf.prepare(db, listTokensQry);
	statements.getToken = await dbf.prepare(db, getTokenQry);
	statements.removeToken = await dbf.prepare(db, removeTokenQry);
	statements.shareToken = await dbf.prepare(db, shareTokenQry);
	statements.listShared = await dbf.prepare(db, listSharedTokensQry);
	
	// Fixes
	statements.getLock = await dbf.prepare(db, getTokenLockQry);
	statements.setLock = await dbf.prepare(db, setTokenLockQry);
	statements.createFix = await dbf.prepare(db, createFixQry);
	statements.getFix = await dbf.prepare(db, getFixQry);
	statements.listFixes = await dbf.prepare(db, listFixesQry);
};

exports.fix = {};

exports.fix.add = async function (fixData) {
	return await dbf.run(statements.createFix, fixData);
};

exports.fix.get = async function (fixData) {
	return await dbf.all(statements.getFix, {
		$fix_id: fixData.$fix_id,
		$token_id: fixData.$token_id
	});
}

exports.fix.list = async function (fixData) {
	let params = dbf.limitOffset(fixData);
	params.$token_id = fixData.$token_id;
	params.$tstamp0 = fixData.$tstamp0;
	params.$tstamp1 = fixData.$tstamp1;
	return await dbf.all(statements.listFixes, params);
};

exports.token = {};

exports.token.add = async function (tokenData) {
	const token_id = crypto.randomBytes(4).toString('hex');
	const params = {
		$token_id: token_id,
		$user_id: tokenData.$user_id,
		$metadata: tokenData.$metadata
	};
	await dbf.run(statements.createToken, params);
	return params.$token_id;
};

exports.token.update = async function (tokenData) {
	return await dbf.run(statements.updateToken, tokenData, "changes");
};

exports.token.setLock = async function (tokenData) {
	return await dbf.run(statements.setLock, tokenData, "changes");
};

exports.token.getLock = async function (tokenData) {
	const res = await dbf.all(statements.getLock, tokenData);
	return res && res[0] || undefined;
};

exports.token.list = async function (tokenData) {
	let params = dbf.limitOffset(tokenData);
	params.$user_id = tokenData.$user_id;
	return await dbf.all(statements.listTokens, params);
};

exports.token.get = async function (tokenData) {
	const res = await dbf.all(statements.getToken, tokenData);
	return res && res[0] || undefined;
};

exports.token.remove = async function (tokenData) {
	return await dbf.run(statements.removeToken, tokenData, "changes");
};

exports.token.share = async function (tokenData) {
	const rtoken = await exports.token.get({
		$token_id: tokenData.$token_id,
		$user_id: tokenData.$user_id
	});
	
	if (rtoken) {
		const $v_token_id = crypto.randomBytes(4).toString('hex');
		const $permissions = [...tokenData.$permissions]
		.filter( p => rtoken.permissions.includes(p) )
		.join("");

		let params = Object.assign({}, tokenData, { $v_token_id, $permissions });
		if (await dbf.run(statements.shareToken, params)) {
			return $v_token_id;
		}
	}
	// Else return undefined
};

exports.token.listShared = async function (tokenData) {
	let params = dbf.limitOffset(tokenData);
	params.$user_id = tokenData.$user_id;
	params.$token_id = tokenData.$token_id;
	return await dbf.all(statements.listShared, params);
};
