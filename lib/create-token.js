const jwt = require('jsonwebtoken');
const cfg = require('./config');
const jwt_secret = cfg.server.secret;

module.exports = function createToken (userDetails) {
	const payload = {
		user_id: userDetails.user_id,
		handle: userDetails.handle
	};
	const options = { expiresIn: "2h" };
	return jwt.sign(payload, jwt_secret, options);
};

