#!/usr/bin/env node

/**
 * @module Main entry point
 */

const http = require('http');
const express = require('express');
const bodyParser  = require("body-parser");
const app = express();
const cfg = require('./lib/config');
const db = require('./lib/db');
const ws = require('./lib/ws');

const server = http.createServer(app);

app.set('x-powered-by', false);
app.set('trust proxy', 'loopback, uniquelocal');
app.ws = ws(server);

// Start websocket queue handler
app.ws.senderStart();

// Shutdown application
app.shutdown = () => {
	// Stop websocket queue handler
	app.ws.senderStop();
};

/*
 * The following two lines load an API version.
 * Repeat for each different supported version.
 */
const api_v0 = require('./api/v0/routes.js');
app.use('/api/v0', api_v0);

// Redirect to a “canonical” API version.
app.get('/api/', (req, res, next) => res.redirect('v0/'));
app.get('/api', (req, res, next) => res.redirect('/api/'));
app.get('/', (req, res, next) => res.redirect('/api/'));

// Generic error handler. Stops stack dumps
// being sent to clients.
app.use(function (err, req, res, next) {
	if (err && err.type == "entity.too.large") {
		res.status(413).send(err);
	} else {
		console.error(err.stack);
		res.status(500).send('General internal error');
	}
});

const verbose = (process.env.NODE_VERBOSE !== undefined) ?
	process.env.NODE_VERBOSE
	: cfg.server.verbose;

if (verbose) {
	app.use(function logRequest(req, res, next) {
		setTimeout(function () {
			var logMessage = `${(new Date()).toISOString()}: [${req.user && (req.user.handle || req.user.token) || '?' }] ${res.statusCode || ""} ${req.method.toUpperCase()} ${req.originalUrl}`;
			console.log(logMessage);
		}, 500);
		next();
	});
}

if (!module.parent) {
	db.initialiseDatabase()
	.then( () => {
		var port = process.env.FOLLOW_SERVER_PORT || cfg.server.port;
		if (process.env.NODE_ENV == "production") {
			// In production, listen only on internal interfaces.
			var os = require('os');
			var ip = require('ip');
			var ifaces = process.env.FOLLOW_SERVER_ADDR &&
				process.env.FOLLOW_SERVER_ADDR.split(/\s+/) ||
				cfg.server.addr;
			if (ifaces) {
				ifaces
				.forEach(addr => {
					console.log(`API started on ${addr} port ${port}`);
					server.listen(port, addr);
				});
			} else {
				ifaces = os.networkInterfaces();
				Object.keys(ifaces)
				.forEach(key =>
					ifaces[key].filter(i =>
						ip.isPrivate(i.address) || i.internal
					).forEach(i => {
						// We have seen “link-local-like” scope id 3
						// in the production servers.
						// https://www.ietf.org/mail-archive/web/roll/current/msg07831.html
						// The !i.scopeid takes care of 0 and undefined, the
						// latter being the case for IPv4 addresses.
						var addr = (!i.scopeid? i.address : `${i.address}%${key}`);
						console.log(`API started on ${addr} port ${port}`);
						server.listen(port, addr);
					})
				);
			}
		} else if (process.env.NODE_ENV == "test" || process.env.NODE_ENV == "development") {
			// Let the process run for this amount of time, then
			// exit.
			const killDelay = cfg.server.killDelay;
			
			// During testing, listen on all interfaces
			const port = process.env.FOLLOW_SERVER_PORT || cfg.server.port;
			const addr = process.env.FOLLOW_SERVER_ADDR;
			server.listen(port, addr);
			console.log(`API started in ${process.env.NODE_ENV} mode on port ${port}`);
			console.log(`Automatic shutdown in ${killDelay / 1000} seconds`);
			setTimeout(() => {
				console.log("Ending process (auto shutdown)");
				process.exit();
			}, killDelay);
		}
	})
	.catch( err => {
		console.error("Database initialisation error", err);
	});
} else if (process.env.NODE_ENV == "test") {
	console.log('API called in test mode');
	app.initialiseDatabase = db.initialiseDatabase;
	app.clearDatabase = db.clearDatabase;
	module.exports = app;
}

