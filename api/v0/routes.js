// #!/usr/bin/env node

const express = require('express');
const bodyParser  = require("body-parser");
const path = require('path');
const cfg = require('../../lib/config');

const router = express.Router();
router.map = require('../../lib/router-map');

const jsonParser = bodyParser.json({strict:false, limit: cfg.server.max_upload_size});
// const rawParser = bodyParser.raw({type: '*/*'});

const mw = require('../../lib/mw');

// router.all('*', mwAdmin.setHeaders);

if (cfg.server.static_assets) {
	const staticPath = path.resolve(__dirname, "../../", cfg.server.static_assets);
	router.use('/static', express.static(staticPath));
}
	
router.map({
	'/version': {
		get: mw.version("v0")
	},
	'/user': {
		post: [ jsonParser, mw.user.create ],
		delete: [ mw.user.jwt, mw.user.remove ],
		'/login': {
			post: [ jsonParser, mw.user.login ]
		},
		'/handle': {
			'/:handle': {
				get: mw.user.get.handle
			}
// 		},
// 		'/:user_id': {
// 			get: mw.user.get,
// 			'/secret': {
// 				post: [ jsonParser, mw.user.set.secret ]
// 			},
// 		   '/handle': {
// 				post: [ jsonParser, mw.user.set.handle ]
// 		   },
// 		   '/metadata': {
// 				post: [ jsonParser, mw.user.set.metadata ]
// 		   }
		}
	},
	'/token': {
		post: [ jsonParser, mw.user.jwt, mw.token.create ],
		get: [ mw.user.jwt, mw.token.list ],
		'/:token_id': {
			get: [ mw.user.jwt, mw.token.get ],
			post: [ jsonParser, mw.user.jwt, mw.token.update ],
			delete: [mw.user.jwt, mw.token.remove ],
	// 		'/sign/:user_id': [ mw.user.jwt, mw.token.sign ],
			'/share': {
				get: [ mw.user.jwt, mw.token.share.list ],
				'/:recipient_id/:permissions([DUcruds]+)/:expiry?': {
					post: [ /*jsonParser,*/ mw.user.jwt, mw.token.share.new ]
				}
			},
			'/fixes': {
				'.(geo)?json': {
					get: [ mw.user.jwt, mw.fix.geojson.list ],
					post: [ jsonParser, mw.user.jwt, mw.fix.geojson.new ]
				},
				get: [ mw.user.jwt, mw.fix.list ],
				post: [ jsonParser, mw.user.jwt, mw.fix.new ],
			},
	// 		'/:fix_id': {
	// 			get: mw.fix.get,
	// 			delete: mw.fix.delete
	// 		},
			'/save/:units?': {
				get: [ mw.fix.new ],
				post: [ jsonParser, mw.user.jwt, mw.fix.new ],
			}
		},
	},
	'/': {
		get: (req, res, next) => { res.sendStatus(204 /*No content*/); next() }
	}
});

module.exports = router;
