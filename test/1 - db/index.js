
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
const promised = require("chai-as-promised");
const db = require('../../lib/db');

chai.use(promised);

describe('Database', () => {
	
	it('should have a user property', () => {
		db.should.have.property('user');
	});
	
	it('should have a token property', () => {
		db.should.have.property('token');
	});
	
	it('should have a fix property', () => {
		db.should.have.property('fix');
	});
	
	describe('Initialisation', () => {
		it('should initialise', async () => {
			await db.initialiseDatabase().should.be.fulfilled;
		})
	});

	describe('Auth database', () => {
			
		const userData = {
			$handle: "test",
			$password: "123456"
		};
		
		const anotherUserData = {
			$handle: "recipient",
			$password: "654321"
		};
		
		const invalidUserData = {
			$handle: "nobody",
			$password: "123456"
		};
		
		const invalidPasswordData = {
			$handle: "test",
			$password: "1234567"
		};
		
		describe('Insert', () => {
			
			it('should create new users', async () => {
				await db.user.add(userData).should.be.fulfilled;
				await db.user.add(anotherUserData).should.be.fulfilled;
			});
			
			it('should not create users without a user name', async () => {
				await db.user.add({$password: "noHandle"}).should.be.rejected;
			});
			
			it('should not create users without a password', async () => {
				await db.user.add({$handle: "noPassword"}).should.be.rejected;
			});
			
			it('should not create users with duplicate handles', async () => {
				await db.user.add(userData).should.be.rejected;
			});
		});
		
		describe('Login', () => {
			
			it('should accept a valid user', async () => {
				const userDetails = await db.user.login(userData).should.be.fulfilled;
				expect(userDetails).to.exist;
			});
			
			it('should reject an invalid user', async () => {
				const userDetails = await db.user.login(invalidUserData).should.be.fulfilled;
				expect(userDetails).to.not.exist;
			});
			
			it('should reject an invalid password', async () => {
				const userDetails = await db.user.login(invalidPasswordData).should.be.fulfilled;
				expect(userDetails).to.not.exist;
			});
			
			it('should return an eight-character user ID', async () => {
				const userDetails = await db.user.login(userData).should.be.fulfilled;
				userDetails.should.have.property('user_id');
				userDetails.user_id.should.be.a('string');
				userDetails.user_id.should.have.lengthOf(8);
			});
		});
		
		describe('Public user', async () => {
			it('there should be an entry for the public user', async () => {
				const publicUser = await db.user.validate({$user_id: "00000000"}).should.be.fulfilled;
				expect('publicUser').to.exist;
				publicUser.should.have.property('user_id');
				publicUser.user_id.should.eql("00000000");
			});
		});
	});

	describe('Locations database', () => {
		
		let tokenData;
		let userId;
		let anotherUserId;
		let tokenId, tokenIdToBeDeleted, sharedTokenId;
		
		before('Get some user data', async () => {
			const userDetails = await db.user.validate({$handle: "test"});
			const anotherUserDetails = await db.user.validate({$handle: "recipient"});

			tokenData = {
				$user_id: userDetails.user_id,
				$metadata: JSON.stringify({smile: "☺"})
			};
			userId = userDetails.user_id;
			anotherUserId = anotherUserDetails.user_id;
		});
		
		describe('Tokens', () => {
			
			describe('Insert', () => {
				
				it('should create tokens', async () => {
					tokenId = await db.token.add(tokenData).should.be.fulfilled;
				});
				
				it('should return an eight-character token', async () => {
					expect(tokenId).to.be.a('string').and.have.lengthOf(8);
				});
				
				it('should not create a token for a non-existing user', async () => {
					const tokenData = {
						$user_id: "deadbeef",
						$metadata: JSON.stringify({frown: "☹"})
					};
					await db.token.add(tokenData).should.be.rejected;
				});
				
			});
			
			describe('List', () => {
				
				let tokenList;
				
				before('insert another token', async () => {
					tokenIdToBeDeleted = await db.token.add(tokenData);
					
					for (let n=0; n < 1000; n++) {
						await db.token.add(tokenData);
					}
				});
				
				it('should list known tokens', async () => {
					tokenList = await db.token.list({$user_id: userId}).should.be.fulfilled;
				});
				
				it('should return an array with 100 items', async () => {
					expect(tokenList).to.be.an('array').and.have.lengthOf(100);
				});
				
				it('every item should have an `token_id` property', async () => {
					tokenList.every( token => token.hasOwnProperty("token_id") ).should.be.true;
				});
				
				it('every item should have a `user_id` property', async () => {
					tokenList.every( token => token.hasOwnProperty("user_id") ).should.be.true;
				});
				
				it('should accept a $limit option', async () => {
					tokenList = await db.token.list({$user_id: userId, $limit: 500}).should.be.fulfilled;
					expect(tokenList).to.be.an('array').and.have.lengthOf(500);
				});
				
				it('should accept an $offset option', async () => {
					tokenList = await db.token.list({$user_id: userId, $offset: 500}).should.be.fulfilled;
					expect(tokenList).to.be.an('array');
					// Returns tokens in chronological order, so the first token
					// to have been inserted should not appear in this batch.
					tokenList.every( token => token.token_id != tokenId ).should.be.true;
				});
				
				it('should not return more than 1000 records in one batch', async () => {
					tokenList = await db.token.list({$user_id: userId, $limit: 5000}).should.be.fulfilled;
					expect(tokenList).to.be.an('array').and.have.lengthOf(1000);
				});
			});
			
			describe('Get', () => {
				it('should fetch tokens by ID and user', async () => {
					const tokenData = {$token_id: tokenId, $user_id: userId};
					let token = await db.token.get(tokenData).should.be.fulfilled;
					expect(token).to.be.an('object').and.have.property('token_id').to.equal(tokenId);
				});
				
				it('should not fetch another user\'s token', async () => {
					const tokenData = {$token_id: tokenId, $user_id: "11111111"};
					let token = await db.token.get(tokenData).should.be.fulfilled;
					expect(token).to.not.exist;
				});
			});
			
			
			describe('Update', () => {
				it('should update an existing token', async () => {
					const tokenData = {$token_id: tokenId, $user_id: userId, $metadata: "Follow"};
					const changes = await db.token.update(tokenData).should.be.fulfilled;
					expect(changes).to.be.a('number').eql(1);
				});
				
				it('should not update a non-existing token', async () => {
					const tokenData = {$token_id: "deadbeef", $user_id: userId, $metadata: "Follow"};
					const changes = await db.token.update(tokenData).should.be.fulfilled;
					expect(changes).to.be.a('number').eql(0);
				});

				it('should not update a token from another user', async () => {
					const tokenData = {$token_id: tokenId, $user_id: "11111111", $metadata: "No follow"};
					const changes = await db.token.update(tokenData).should.be.fulfilled;
					expect(changes).to.be.a('number').eql(0);
				});
				
				it('should return the modified data', async () => {
					const tokenData = {$token_id: tokenId, $user_id: userId};
					let token = await db.token.get(tokenData).should.be.fulfilled;
					expect(token).to.be.an('object').and.have.property('metadata').to.equal('Follow');
				});
				
				it('should lock a token', async () => {
					let changes = await db.token.setLock({$token_id: tokenIdToBeDeleted, $lock_key: "🔑"});
					expect(changes).to.be.a('number').and.eql(1);
				});
				
				it("should retrieve a locked token's key", async () => {
					let res = await db.token.getLock({$token_id: tokenIdToBeDeleted});
					res.should.be.an('object');
					res.should.have.property('lock_key');
					res.lock_key.should.eql("🔑");
				});
			});
			
			
			describe('Delete', () => {
				
				it('should delete tokens by ID and user', async () => {
					const tokenData = {$token_id: tokenIdToBeDeleted, $user_id: userId};
					await db.token.remove(tokenData).should.be.fulfilled;
					let token = await db.token.get(tokenData).should.be.fulfilled;
					expect(token).to.not.exist;
				});
				
				it('should not delete another user\'s token', async () => {
					const riteTokenData = {$token_id: tokenId, $user_id: userId};
					const rongTokenData = {$token_id: tokenId, $user_id: "11111111"};
					await db.token.remove(rongTokenData).should.be.fulfilled;
					let token = await db.token.get(riteTokenData).should.be.fulfilled;
					expect(token).to.be.an('object').and.have.property('token_id').to.equal(tokenId);
				});
			});
			
			
			describe('Share', () => {
				
				it('should share an existing token', async () => {
					const shareParams = {
						$token_id: tokenId,
						$user_id: userId,
						$recipient_id: anotherUserId,
						$permissions: "rs"
					};
					sharedTokenId = await db.token.share(shareParams).should.be.fulfilled;
					expect(sharedTokenId).to.be.a('string').and.have.lengthOf(8);
					
					const tokenData = {$token_id: sharedTokenId, $user_id: anotherUserId};
					let token = await db.token.get(tokenData).should.be.fulfilled;
					expect(token).to.be.an('object')
					.and.have.property('token_id')
					.to.equal(sharedTokenId);
				});
				
				it('should list the handle of the token sharer', async () => {
					const tokenData = {$token_id: sharedTokenId, $user_id: anotherUserId};
					let token = await db.token.get(tokenData).should.be.fulfilled;
					expect(token).to.be.an('object')
					.and.have.property('shared_by')
					.to.equal("test");
				});
				
				it('the owner should be able to unshare a token', async () => {
					const tokenData = {$token_id: sharedTokenId, $user_id: userId};
					await db.token.remove(tokenData).should.be.fulfilled;
					let token = await db.token.get(tokenData).should.be.fulfilled;
					expect(token).to.not.exist;
				});
				
			});
			
		});
		
		describe('Fixes', () => {
			
			describe('Insert', () => {
				
				const fixData = {
					$latitude: 0,
					$longitude: 0,
					$altitude: 0,
					$tstamp: 0,
					$speed: 0,
					$direction: 0,
					$metadata: ""
				};
				
				let fixId;
				
				it('should create fixes', async () => {
					fixId = await db.fix.add({$token_id: tokenId, ...fixData}).should.be.fulfilled;
				});
				
				it('should return a numeric ID', async () => {
					expect(fixId).to.be.a('number');
				});
				
				it('should require a token', async () => {
					await db.fix.add(fixData).should.be.rejected;
				});
				
				it('should reject an unknown token', async () => {
					await db.fix.add({$token_id: "deadbeef", ...fixData}).should.be.rejected;
				});
			});
		});
		
	});

});
