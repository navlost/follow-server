
const URL = require('url');
const JWT = require('jsonwebtoken');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
const chaiHttp = require('chai-http');
const promised = require("chai-as-promised");
const server = require('../../');

const cfg = require('../../lib/config');

chai.use(promised);
chai.use(chaiHttp);

describe('API', () => {
	
	let versionPath;

	const login = {
		handle: "test",
		password: "123"
	};
	
	const anotherLogin = {
		handle: "recipient",
		password: "321"
	};
	
	let anotherUserId, recipientId, publicId = "00000000";
	
	let jwt, anotherJwt;
	
	let tokenId, tokenIdToBeDeleted, sharedTokenId, publicSharedTokenId;
	
	before('Initialise and clear databases', async () => {
		await server.clearDatabase();
	});
	
	after('Shut down server', async () => {
		await server.shutdown();
	});
	
	describe('Discoverability', () => {
		
		describe('Root path', () => {
			it('should redirect to API', async () => {
				const res = await chai.request(server).get('/');
				res.should.redirect;
				versionPath = URL.parse(res.redirects.pop()).pathname;
				expect(versionPath).to.exist;
			});
			
		});
		
		describe('GET /version', () => {
			it('should return API version', async () => {
				const res = await chai.request(server).get(versionPath+"version")
				res.should.have.status(200);
				res.should.be.json;
				res.body.should.have.property('version');
			});
		});
		
	});
	
	describe('User management', () => {
		
		describe('POST /user', () => {
			
			it('should create a user', async () => {
				const res = await chai.request(server)
					.post(versionPath+"user")
					.type("json")
					.send(login);
				res.should.have.status(201);
			});
			
		});
		
		describe('POST /user/login', () => {
			
			it('should log the user in', async () => {
				const res = await chai.request(server)
					.post(versionPath+"user/login")
					.type('json')
					.send(login);
					
				res.should.have.status(200);
				res.should.be.json;
				res.body.should.have.property('jwt').a('string');
				res.should.have.header('x-jwt-token');
				
				jwt = res.headers['x-jwt-token'];
			});
			
			it('should reject a wrong password', async () => {
				const res = await chai.request(server)
					.post(versionPath+"user/login")
					.type('json')
					.send({...login, password: "321"});
					
				res.should.have.status(401);
			});
			
		});
		
		describe('DELETE /user', () => {
			
			it('should require a user to be logged in', async () => {
				const res = await chai.request(server)
					.del(versionPath+"user");
					
				res.should.have.status(401);
			});
			
			it('should delete the logged in user', async () => {
				const res = await chai.request(server)
					.del(versionPath+"user")
					.set('Authorization', 'Bearer ' + jwt);
					
				res.should.have.status(204);
			});
			
		});
		
	});
	
	describe('Token management', () => {
		
		before('Create users', async () => {
			
			const res0 = await chai.request(server)
				.post(versionPath+"user")
				.type("json")
				.send(login);
			res0.should.have.status(201);
			
			const res1 = await chai.request(server)
				.post(versionPath+"user/login")
				.type('json')
				.send(login);
				
			res1.should.have.status(200);
			res1.should.be.json;
			res1.body.should.have.property('jwt').a('string');
			res1.should.have.header('x-jwt-token');
			
			jwt = res1.headers['x-jwt-token'];
			
			// Creates second user for testing token sharing
			const res2 = await chai.request(server)
				.post(versionPath+"user")
				.type("json")
				.send(anotherLogin);
			res2.should.have.status(201);
			
			const res3 = await chai.request(server)
				.post(versionPath+"user/login")
				.type('json')
				.send(anotherLogin);
				
			res3.should.have.status(200);
			res3.should.be.json;
			res3.body.should.have.property('jwt').a('string');
			res3.should.have.header('x-jwt-token');
			
			anotherJwt = res3.headers['x-jwt-token'];
			const anotherUser = JWT.verify(anotherJwt, cfg.server.secret);
			recipientId = anotherUser.user_id;
			recipientId.should.be.a('string').and.have.lengthOf(8);
			
		});
		
		describe('POST /token', () => {
			
			it('should create a token', async () => {
				const res = await chai.request(server)
					.post(versionPath+"token")
					.set('Authorization', 'Bearer ' + jwt)
					.type("json")
					.send();
				res.should.have.status(201);
				res.should.be.json;
				res.body.should.be.a('string');
			});
			
			it('should not create a token without a valid user', async () => {
				const res = await chai.request(server)
					.post(versionPath+"token")
					.type("json")
					.send();
				res.should.have.status(401);
			});
			
			it('should not accept too large metadata', async () => {
				const res = await chai.request(server)
					.post(versionPath+"token")
					.set('Authorization', 'Bearer ' + jwt)
					.type("json")
					.send({metadata: {string: "Too long!".repeat(500)}});
				res.should.have.status(413);
			});
			
		});
		
		describe('GET /token', () => {
			
			before('create some tokens', async () => {
				for (let n=0; n<500; n++) {
					const res = await chai.request(server)
						.post(versionPath+"token")
						.set('Authorization', 'Bearer ' + jwt)
						.type("json")
						.send({metadata: {n}});
				}
				
			});
			
			it('should list existing tokens', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token")
					.set('Authorization', 'Bearer ' + jwt);
				res.should.have.status(200);
				res.body.should.be.an('array').and.have.lengthOf.at.least(1);
				tokenId = res.body[0].token_id;
				tokenIdToBeDeleted = res.body[1].token_id;
			});
			
			it('should not require a login', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token")
				res.should.have.status(200);
			});
			
			it('should not return non-public tokens if not logged in', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token")
				res.should.have.status(200);
				res.body.should.be.an('array').and.have.lengthOf(0);
			});
			
			it('should accept a `limit` parameter', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token?l=10")
					.set('Authorization', 'Bearer ' + jwt);
				res.should.have.status(200);
				res.body.should.be.an('array').and.have.lengthOf.at.most(10);
			});
			
			it('should also accept an `offset` parameter', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token?l=10&o=20")
					.set('Authorization', 'Bearer ' + jwt);
				res.should.have.status(200);
				res.body.should.be.an('array').and.have.lengthOf.at.most(10);
				res.body.every(item => item.metadata.n > 18).should.be.true;
				// Why 18? Because the first item has no `n` and the count
				// is zero-based, so the sequence is: undefined, 0, 1, … 18
				// for the first twenty elements.
			});
			
		});
		
		describe('GET /token/:token', () => {
			
			it('should require a valid user', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token/"+tokenId);
					
				res.should.have.status(404);
			});
			
			it('should return a token', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token/"+tokenId)
					.set('Authorization', 'Bearer ' + jwt);
					
				res.should.have.status(200);
				res.body.should.be.an('object');
				res.body.should.have.property('token_id').eql(tokenId);
			});
			
			it('should not find a non-existing token', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token/"+"ZZZZZZZZZZZZ")
					.set('Authorization', 'Bearer ' + jwt);
					
				res.should.have.status(404);
			});
			
		});
		
		describe('POST /token/:token', () => {
			
			it('should require a valid user', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token/"+tokenId);
					
				res.should.have.status(404);
			});
			
			it('should accept modifications to a token', async () => {
				
				const res = await chai.request(server)
				.post(versionPath+"token/"+tokenId)
				.set('Authorization', 'Bearer ' + jwt)
				.type("json")
				.send({metadata: {name: "Follow"}});
				
				res.should.have.status(204);
			});
			
			it('should return the modified token', async () => {
				const res = await chai.request(server)
				.get(versionPath+"token/"+tokenId)
				.set('Authorization', 'Bearer ' + jwt);
				
				res.body.should.have.property('metadata')
				res.body.metadata.should.have.property('name')
				res.body.metadata.name.should.be.eql("Follow");
			});
		});
		
		describe('POST /token/:token/share/:recipient_id/:permissions/:expiry', async () => {
			
			it('should share a token with another user', async () => {
				const res = await chai.request(server)
				.post(`${versionPath}token/${tokenId}/share/${recipientId}/rs`)
				.set('Authorization', 'Bearer ' + jwt);
				
				res.should.have.status(201);
				res.body.should.be.an('object');
				res.body.should.have.property('token_id');
				res.body.token_id.should.be.a('string').and.have.lengthOf(8);
				
				sharedTokenId = res.body.token_id;
			});
			
			it('the recipient should see the token', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token")
					.set('Authorization', 'Bearer ' + anotherJwt);
				res.should.have.status(200);
				res.body.should.be.an('array').and.have.lengthOf.at.least(1);
				res.body.filter(t => t.token_id == sharedTokenId).should.have.lengthOf(1);
			});
			
			it('the recipient should see contents of the token', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token/"+sharedTokenId)
					.set('Authorization', 'Bearer ' + anotherJwt);
					
				res.should.have.status(200);
				res.body.should.be.an('object');
				res.body.should.have.property('token_id').eql(sharedTokenId);
			});
			
			describe('Globally visible shares', async () => {
				
				it('should share a token with the public user, globally visible', async () => {
					const res = await chai.request(server)
					.post(`${versionPath}token/${tokenId}/share/${publicId}/rs/`)
					.set('Authorization', 'Bearer ' + jwt);
					
					res.should.have.status(201);
					res.body.should.be.an('object');
					res.body.should.have.property('token_id');
					res.body.token_id.should.be.a('string').and.have.lengthOf(8);
					
					publicSharedTokenId = res.body.token_id;
				});
				
				it('an unauthenticated user should see a globally visible public token', async () => {
					const res = await chai.request(server)
						.get(versionPath+"token");
					res.should.have.status(200);
					res.body.should.be.an('array').and.have.lengthOf.at.least(1);
					res.body.filter(t => t.token_id == publicSharedTokenId).should.have.lengthOf(1);
				});
				
				it('an unauthenticated user should see the contents of a globally visible public token', async () => {
					const res = await chai.request(server)
						.get(versionPath+"token/"+publicSharedTokenId);
						
					res.should.have.status(200);
					res.body.should.be.an('object');
					res.body.should.have.property('token_id').eql(publicSharedTokenId);
				});
				
			});
			
			describe('Restricted visibility shares', async () => {
				
				it('should share a token with the public user, restricted', async () => {
					const res = await chai.request(server)
					.post(`${versionPath}token/${tokenId}/share/${publicId}/r/`)
					.set('Authorization', 'Bearer ' + jwt);
					
					res.should.have.status(201);
					res.body.should.be.an('object');
					res.body.should.have.property('token_id');
					res.body.token_id.should.be.a('string').and.have.lengthOf(8);
					
					publicSharedTokenId = res.body.token_id;
				});
				
				it('an unauthenticated user should not see a restricted public token', async () => {
					const res = await chai.request(server)
						.get(versionPath+"token");
					res.should.have.status(200);
					res.body.should.be.an('array').and.have.lengthOf.at.least(1);
					res.body.filter(t => t.token_id == publicSharedTokenId).should.have.lengthOf(0);
				});
				
				it('an unauthenticated user should see the contents of a restricted public token', async () => {
					const res = await chai.request(server)
						.get(versionPath+"token/"+publicSharedTokenId);
						
					res.should.have.status(200);
					res.body.should.be.an('object');
					res.body.should.have.property('token_id').eql(publicSharedTokenId);
				});
				
			});
			
		});
		
		describe('POST /token/:token/share', async () => {
			
			it('should get a list of shares', async () => {
				const res = await chai.request(server)
				.get(`${versionPath}token/${tokenId}/share`)
				.set('Authorization', 'Bearer ' + jwt);
				
				res.should.have.status(200);
				res.body.should.be.an('array').and.have.lengthOf.at.least(1);
			});
			
		});
		
		describe('DELETE /token/:token', () => {
			
			it('should require a valid user', async () => {
				const res = await chai.request(server)
					.del(versionPath+"token/"+tokenIdToBeDeleted);
					
				res.should.have.status(401);
			});
			
			it('should delete the referenced token', async () => {
				const res = await chai.request(server)
					.del(versionPath+"token/"+tokenIdToBeDeleted)
					.set('Authorization', 'Bearer ' + jwt);
					
				res.should.have.status(204);
				// NOTE: HTTP 204 is returned even if the token
				// did not exist to start with. This is a feature.
			});
			
			it('should not exist after deletion', async () => {
				const res = await chai.request(server)
					.get(versionPath+"token/"+tokenIdToBeDeleted)
					.set('Authorization', 'Bearer ' + jwt);
					
				res.should.have.status(404);
			});
			
			it('should not let unauthenticated users delete public tokens', async () => {
				const res = await chai.request(server)
					.del(versionPath+"token/"+tokenIdToBeDeleted);
					
				res.should.have.status(401);
			});
			
		});
		
	});
	
	describe('Fix handling', () => {
		
		const fixData = {
			id: "🔑",
			latitude: 0,
			longitude: 0,
			altitude: 0,
			tstamp: 0,
			speed: 0,
			direction: 0,
		};

		
		describe('POST /token/:token/save', () => {
			
			it('should NOT require a valid user', async () => {
				
				const res = await chai.request(server)
					.post(versionPath+"token/"+tokenId+"/save")
					.type("json")
					.send(fixData);
					
					
				res.should.have.status(201);
			});
			
			it('should require a valid token', async () => {
				
				const res = await chai.request(server)
					.post(versionPath+"token/ZZZZZZZZZZZZ/save")
					.set('Authorization', 'Bearer ' + jwt)
					.type("json")
					.send(fixData);
					
					
				res.should.have.status(404);
			});
			
			it('should create a fix', async () => {
				
				const res = await chai.request(server)
					.post(versionPath+"token/"+tokenId+"/save")
					.set('Authorization', 'Bearer ' + jwt)
					.type("json")
					.send(fixData);
					
				res.should.have.status(201);
				res.should.be.json;
				res.body.should.be.an('array').and.have.lengthOf(1)
				res.body[0].should.have.property('fix_id');
				res.body[0].fix_id.should.be.a('number').at.least(0);
			});
			
			it('should prevent writing to a token with the wrong key…', async () => {
				const fixWithWrongKey = Object.assign({}, fixData, {id: "🗝"});
				const res = await chai.request(server)
					.post(versionPath+"token/"+tokenId+"/save")
					.type("json")
					.send(fixWithWrongKey);
					
				res.should.have.status(202);
			});
			
			it('…unless using a valid authorisation token', async () => {
				const fixWithWrongKey = Object.assign({}, fixData, {id: "🗝"});
				const res = await chai.request(server)
					.post(versionPath+"token/"+tokenId+"/save")
					.set('Authorization', 'Bearer ' + jwt)
					.type("json")
					.send(fixWithWrongKey);
					
				res.should.have.status(201);
			});
			
		});
		
		describe('GET /token/:token/save', () => {
			
			let url;
			
			before('build save URL', () => {
				url = new URL.URL(`${versionPath}token/${tokenId}/save`, "relative://");
				url.search = new URL.URLSearchParams(fixData);
				url = url.href.replace(/^relative:\/\//, "");
			});
			
			it('should require a valid token', async () => {
				
				const res = await chai.request(server)
					.get(versionPath+"token/ZZZZZZZZZZZZ/save")
					.set('Authorization', 'Bearer ' + jwt);
					
					
				res.should.have.status(404);
			});
			
			it('should NOT require a valid user', async () => {
				
				const res = await chai.request(server)
					.get(url);
					
					
				res.should.have.status(201);
			});
			
			it('should create a fix', async () => {
				
				const res = await chai.request(server)
					.get(url)
					.set('Authorization', 'Bearer ' + jwt);
					
				res.should.have.status(201);
				res.should.be.json;
				res.body.should.be.an('array').and.have.lengthOf(1)
				res.body[0].should.have.property('fix_id');
				res.body[0].fix_id.should.be.a('number').at.least(0);
			});
		});
		
		describe ('GET /token/:token/fixes', () => {
			
			let url;
			let ts = Date.now();
			
			before('build URL', async () => {
				url = `${versionPath}token/${tokenId}/fixes`;
			});
			
			before('prime database', async () => {
				const db = require('../../lib/db');
				let fixData = {
					$token_id: tokenId,
					$latitude: 0,
					$longitude: 0,
					$altitude: 0,
					$tstamp: 0,
					$speed: 0,
					$direction: 0,
					$metadata: ""
				};
				
				const ts1 = ts;
				const ts0 = ts1 - 86400000 * 14; // Two weeks ago
				const inc = 1800000; // 30 minutes
				
				for (let t = ts0; t < ts1; t += inc) {
					fixData.$tstamp = t;
					await db.fix.add(fixData);
				}

			});
			
			it ('should get the latest tokens', async () => {
				const res = await chai.request(server)
				.get(url)
				.set('Authorization', 'Bearer ' + jwt);
				
				res.should.have.status(200);
				res.should.be.json;
				res.body.should.be.an('array').and.have.length.at.least(1);
			});
			
			it('should accept timestamp restrictions', async () => {
				const ts0 = ts - 86400000 * 3;
				const ts1 = ts - 86400000 * 2;
				const expected = (ts1-ts0) / 1800000 + 1; // Fencepost problem
				
				const res = await chai.request(server)
				.get(`${url}?ts0=${ts0}&ts1=${ts1}`)
				.set('Authorization', 'Bearer ' + jwt);
				
				res.should.have.status(200);
				res.should.be.json;
				res.body.should.be.an('array').and.have.lengthOf(expected);
			});
			
		});
		
	});
	
});
