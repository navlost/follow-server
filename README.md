# Follow Server

[![pipeline status](https://gitlab.com/navlost/follow-server/badges/devel/pipeline.svg)](https://gitlab.com/navlost/follow-server/commits/devel)
[![coverage report](https://gitlab.com/navlost/follow-server/badges/devel/coverage.svg)](https://gitlab.com/navlost/follow-server/commits/devel)

An asset tracking server.

## Installation

`git clone https://gitlab.com/navlost/follow-server.git`

## Basic usage

### Configuration

* Create a directory where the data will be stored. Data is stored in SQLite database files.

* Optionally, create a directory where static assets will be served from. See the client configuration section below.

* Create a `config.yaml` (or `config.yml`) file in the project's top directory. You may use the provided `config.dev.yml` or `config.test.yml` as a base. You **must** change the provided secrets for production use. One way to generate a secret is `openssl rand -hex 64`.

### Starting the server

Ensure you have defined your configuration first.

`npm start`

By default, in production mode the server will listen on internal interfaces *only* (both IPv4 and IPv6). It is assumed that a proxy such as Nginx will be used to expose the server to the outside world.

## Optional configuration

### Client configuration

By default, the [web frontend](https://gitlab.com/navlost/follow-frontend) uses [OpenStreetMap](https://www.openstreetmap.org/) as its background layer. Server administrators may however provide alternative layers by creating a `map-layers.json` file in the server.

How this works:

1. Create a directory on the server where the file will be stored. This can be a subdirectory of the data folder (*but it must not be the same directory, otherwise anyone will have access to the database files!*).

2. Provide the path to that directory in the `server` / `static_assets` variable of your `config.yml` file. See `config.dev.yml` for an example.

3. Save your `map-layers.json` file in that directory.

#### Example `map-layers.json`

```json
[
    {
        "title": "OpenTopoMap",
        "url": "https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png",
        "options": {
            "attribution": "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors",
            "maxZoom": 17
        }
    },
    {
        "title": "OpenStreetMap",
        "url": "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        "options": {
            "maxZoom": 19
        }
    },
    {
        "title": "Topo Map",
        "url": "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
        "options": {
            "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a>, &copy; <a href=\"https://carto.com/attribution\">CARTO</a>",
            "maxZoom": 20
        }
    }
]
 
```

## More information

Visit [the wiki](https://gitlab.com/navlost/follow-server/wikis/home) for more project details.
